CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

 
INTRODUCTION
------------
The Tailored Content module permits content developers to define tailored variations of individual pages which are displayed at runtime according to individual user variables.

 * For a full description of the module, visit the project page:
 * To submit bug reports and feature suggestions, or to track changes:


REQUIREMENTS
------------
This module requires the following modules:
 * Entity (https://www.drupal.org/project/entity)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * For a full description of module configuration, visit the project page:


TROUBLESHOOTING
---------------
 * TBD


FAQ
---
 * TBD


MAINTAINERS
-----------

Current maintainers:
 * Jeffery McLaughlin (jefferymac) - https://drupal.org/user/54136


This project has been sponsored by:
 * Radiant Creative Group
 * Development of this module was supported in part by the National Cancer Institute of 			
   the National Institutes of Health under Award Number R42CA126453.
