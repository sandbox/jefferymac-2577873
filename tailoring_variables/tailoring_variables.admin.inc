<?php

/**
 * @file
 * This file is used for defining the fuctions which manages the tailoring variable and its thresholds.
 */
function tailoring_variable_admin_page() {
  if (!module_exists('tailoring')) {
    drupal_set_message(t('Please enable "Tailoring Module" Inorder to work this module properly'), 'warning');
  }
  $output = 'Welcome to the administration page for your Tailoring Variables.<br/>';
  $result = load_all_tailoring_variable_ids();
  $caption_table_add = t('<ul class="action-links"><li> <a href="@url">Add Tailoring Variable</a></li></ul>', array('@url' => url('admin/structure/tailoring_variable/add')));
  $headers = array(t('Tailoring variable Name'), t('Actions'));
  if (is_array($result)) {
    $arr_tvs = isset($result) ? array_keys($result) : ' ';
    $tailoring_variables = tailoring_variable_load_multiple($arr_tvs);
    foreach ($tailoring_variables as $each_tv) {
      $link = l(t('View'), 'admin/structure/tailoring_variable/' . $each_tv->tailoring_variable_id);
      $link .= '&nbsp;| &nbsp;';
      $link .= l(t('Edit'), 'admin/structure/tailoring_variable/' . $each_tv->tailoring_variable_id . '/edit');
      $link .= '&nbsp;|&nbsp;';
      $link .= l(t('Delete'), 'admin/structure/tailoring_variable/' . $each_tv->tailoring_variable_id . '/delete');
      $rows[] = array($each_tv->tailoring_variable_name, $link);
      // Format the table array. This is a pretty simple table, you could include
      // links to the field management pages for each bundle too.
    }
  }
  else {
    $rows[] = array('No Variable available');
  }
  $output = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#caption' => $caption_table_add,
  );

  return $output;
}

/**
 * Function used to set title call back in tailoring_variables_menu function.
 */
function tailoring_variable_page_title($tailoring_variable) {
  return $tailoring_variable->tailoring_variable_name;
}

/**
 * Function used to display the tailoring variable's administration page (Used as a call back funtion in tailoring_variables_menu function).
 */
function tailoring_variable_page_view($tailoring_variable, $view_mode = 'full') {
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Administration'), 'admin');
  $breadcrumb[] = l(t('Structure'), 'admin/structure');
  $breadcrumb[] = l(t('Tailoring Variables'), 'admin/structure/tailoring_variable/manage');
  drupal_set_breadcrumb($breadcrumb);
  $wrapper = entity_metadata_wrapper('tailoring_variable', $tailoring_variable);
  $author_name = $wrapper->get('uid')->value()->name;
  $thd_data = $wrapper->get('thresholds')->value();
  $threshold_data = t('<ul class="action-links"><li><a href="@url">Add Threshold</a></li></ul>',
    array('@url' => url('admin/structure/tailoring_variable/' . $tailoring_variable->tailoring_variable_id . '/thresholds/add'))
  );
  $cnt = 1;
  $threshold_data .= '<table><tr><th>No:</th><th>Value</th><th>Name</th><th>Actions</th></tr>';
  if (is_array($thd_data)) {
    $thd_arr = variable_thresholds_load_multiple(array_keys($thd_data));
    foreach ($thd_arr as $key => $thd_obj) {
      $threshold_data .= '<tr><td>' . $cnt . '</td>';
      $threshold_data .= '<td>' . $thd_obj->threshold_values . '</td>';
      $threshold_data .= '<td>' . $thd_obj->threshold_name . '</td><td>';
      $threshold_data .= l(t('Edit'), 'admin/structure/threshold/' . $thd_obj->threshold_id . '/edit/' . $cnt);
      $threshold_data .= '&nbsp;|&nbsp;';
      $threshold_data .= l(t('Delete'), 'admin/structure/threshold/' . $thd_obj->threshold_id . '/delete');
      $threshold_data .= '</td></tr>';
      ++$cnt;
    }
  }
  else {
    $threshold_data .= "<tr><td colspan='4'align='center' >No Thresholds available</td></tr>";
  }
  $threshold_data .= '</table>';
  $tailoring_variable->content = array();
  field_attach_prepare_view('tailoring_variable', array($tailoring_variable->tailoring_variable_id => $tailoring_variable), $view_mode);
  entity_prepare_view('tailoring_variable', array($tailoring_variable->tailoring_variable_id => $tailoring_variable));
  $tailoring_variable->content += field_attach_view('tailoring_variable', $tailoring_variable, $view_mode);
  $tailoring_variable->content['tailoring_variable'] = array(
    '#type' => 'item',
    '#title' => t('Tailoring Variable'),
    '#markup' => $tailoring_variable->tailoring_variable_name,
  );
  $tailoring_variable->content['tailoring_variable_machine_name'] = array(
    '#type' => 'item',
    '#title' => t('Tailoring Variable Machine name'),
    '#markup' => $tailoring_variable->tailoring_variable_machine_name,
  );
  $tailoring_variable->content['tailoring_variable_description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#markup' => $tailoring_variable->tailoring_variable_description,
  );
  $tailoring_variable->content['author'] = array(
    '#type' => 'item',
    '#title' => t('Author'),
    '#markup' => $author_name,
  );
  $tailoring_variable->content['thresholds'] = array(
    '#type' => 'item',
    '#title' => t('Thresholds'),
    '#markup' => $threshold_data,
  );
  drupal_set_title($tailoring_variable->tailoring_variable_name);

  return $tailoring_variable->content;
}

/**
 * Implements hook_field_extra_fields().
 */
function tailoring_variables_field_extra_fields() {
  $return = array();
  $return['tailoring_variable']['tailoring_variable'] = array(
    'form' => array(
      'tailoring_variable_name' => array(
        'label' => t('Tailoring Variable'),
        'description' => t('Tailoring Variable Name'),
        'weight' => -10,
      ),
      'tailoring_variable_machine_name' => array(
        'label' => t('Tailoring Variable Machine Name'),
        'description' => t('Tailoring Variable Machine Name'),
        'weight' => -10,
      ),
      'tailoring_variable_description' => array(
        'label' => t('Tailoring Variable Description'),
        'description' => t('Tailoring Variable Description'),
        'weight' => -9,
      ),
      'created' => array(
        'label' => t('Created date'),
        'description' => t('Tailoring Variable Created date'),
        'weight' => -8,
      ),
      'modified' => array(
        'label' => t('Modified date'),
        'description' => t('Tailoring Variable Modified date'),
        'weight' => -7,
      ),
    ),
  );

  return $return;
}

/**
 * Used for creating a dynamic tailoring variable object during Add/Edit tailoring varaible process.
 * A call back function in tailoring_variables_menu function.
 */
function tailoring_variable_add() {
  global $user;
  if (arg(3) != '' && arg(4) == 'edit') {
    $t_v = tailoring_variable_load(arg(3));
  }
  else {
    $t_v = (object)array(
      'tailoring_variable_id' => '',
      'tailoring_variable_name' => '',
      'tailoring_variable_machine_name' => '',
      'tailoring_variable_description' => '',
      'created' => REQUEST_TIME,
      'modified' => '',
      'uid' => $user->uid,
    );
  }

  return drupal_get_form('tailoring_variable_add_form', $t_v);
}

/**
 * Function used to define form for add/edit tailoring variable.
 */
function tailoring_variable_add_form($form, &$form_state, $t_v) {
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Administration'), 'admin');
  $breadcrumb[] = l(t('Structure'), 'admin/structure');
  $breadcrumb[] = l(t('Tailoring Variables'), 'admin/structure/tailoring_variable/manage');
  drupal_set_breadcrumb($breadcrumb);
  $md_date = '';
  $submit_lbl = '';
  if (!empty($t_v->tailoring_variable_id)) {
    $form['tailoring_variable_id'] = array(
      '#type' => 'value',
      '#value' => $t_v->tailoring_variable_id,
    );
    $md_date = REQUEST_TIME;
    $submit_lbl = 'Save and Edit Thresholds';
  }
  else {
    $md_date = $t_v->modified;
    $submit_lbl = 'Save and Add Thresholds';
  }
  $form['tailoring_variable_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Tailoring variable Name'),
    '#required' => TRUE,
    '#default_value' => $t_v->tailoring_variable_name,
  );
  $form['tailoring_variable_machine_name'] = array(
    '#type' => 'value',
    '#value' => $t_v->tailoring_variable_machine_name,
  );
  $form['tailoring_variable_description'] = array(
    '#type' => 'textarea',
    '#rows' => 4,
    '#cols' => 30,
    '#title' => t('Tailoring variable description'),
    '#required' => TRUE,
    '#default_value' => $t_v->tailoring_variable_description,
  );
  $form['created'] = array(
    '#type' => 'value',
    '#value' => $t_v->created,
  );
  $form['modified'] = array(
    '#type' => 'value',
    '#value' => $md_date,
  );
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $t_v->uid,
  );
  field_attach_form('tailoring_variable', $t_v, $form, $form_state);
  $form['actions']['submit_1'] = array(
    '#type' => 'submit',
    '#value' => t('Save and Exit'),
  );
  $form['actions']['submit_3'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );
  $threshold_data = '';
  if (is_numeric(arg(3))) {
    $tailoring_variable = tailoring_variable_load(arg(3));
    $current_thresholds = threshold_set($tailoring_variable);
    $threshold_data = t('<ul class="action-links"><li> <a href="@url">Add Threshold</a></li></ul>',
      array('@url' => url('admin/structure/tailoring_variable/' . $tailoring_variable->tailoring_variable_id . '/thresholds/add'))
    );
    $threshold_data .= '<table><tr><th>No</th><th>Value</th><th>Name</th><th>Actions</th></tr>';
    if (is_array($current_thresholds)) {
      $thd_objs = variable_thresholds_load_multiple(array_keys($current_thresholds));
      $cnt = 1;
      foreach ($thd_objs as $key => $each_threshold_obj) {
        $threshold_data .= '<tr><td>' . $cnt . '</td>';
        $threshold_data .= '<td>' . $each_threshold_obj->threshold_values . '</td>';
        $threshold_data .= '<td>' . $each_threshold_obj->threshold_name . '</td><td>';
        $threshold_data .= l(t('Edit'), 'admin/structure/threshold/' . $each_threshold_obj->threshold_id . '/edit/' . $cnt);
        $threshold_data .= '&nbsp;|&nbsp;';
        $threshold_data .= l(t('Delete'), 'admin/structure/threshold/' . $each_threshold_obj->threshold_id . '/delete');
        $threshold_data .= '</td></tr>';
        ++$cnt;
      }
    }
    else {
      $threshold_data .= "<tr><td colspan='4' align='center' >No Thresholds available</td></tr>";
    }
    $threshold_data .= '</table>';
    $form['threshold_data'] = array(
      '#type' => 'item',
      '#title' => t('Thresholds'),
      '#markup' => $threshold_data,
    );
  }

  return $form;
}

/**
 * Function used for validating tailoring variable add/edit form.
 */
function tailoring_variable_add_form_validate($form, &$form_state) {
  $tv_submisttion = (object) $form_state['values'];
  if (empty($tv_submisttion->tailoring_variable_id)) {
    if (_validate_tailoring_variable_name($tv_submisttion)) {
      form_set_error('tailoring_variable_name', 'Tailoring variable name "' . $tv_submisttion->tailoring_variable_name . '" already exists.Use different name...');
    }
  }
  field_attach_form_validate('tailoring_variable', $tv_submisttion, $form, $form_state);
}

/**
 * Function used to validate tailoring variable name duplication.
 */
function _validate_tailoring_variable_name($tv_submisttion) {
  $new_tv_name = _create_machine_name($tv_submisttion->tailoring_variable_name);
  $ids = load_all_tailoring_variable_ids();
  if (is_array($ids)) {
    $tv_ids = array_keys($ids);
    $tailoring_variables = tailoring_variable_load_multiple($tv_ids);
    foreach ($tailoring_variables as $key => $each_variable) {
      if ($each_variable->tailoring_variable_machine_name == $new_tv_name) {
        return TRUE;
      }
    }

    return FALSE;
  }
}

/**
 * Function used to create tailoring variable machine name.
 */
function _create_machine_name($new_tv_name) {
  $c_name = strtolower(trim($new_tv_name));
  $c_name = preg_replace('/\s+/', '_', $c_name);

  return $c_name;
}

/**
 * Function used for submitting the tailoring variable add/edit form.
 */
function tailoring_variable_add_form_submit($form, &$form_state) {
  $tv_submisttion = (object) $form_state['values'];
  $tv_submisttion->tailoring_variable_machine_name = _create_machine_name($tv_submisttion->tailoring_variable_name);
  $tv_obj = tailoring_variable_load(arg(3));
  $used_nodes_arr = _check_tailoring_variable_usage($tv_obj);
  $count = count($used_nodes_arr);
  if (is_array($used_nodes_arr) && !empty($used_nodes_arr) && $tv_submisttion->tailoring_variable_name == $tv_obj->tailoring_variable_name) {
    drupal_set_message(filter_xss(_createErrorAndWarningMessages($tv_obj, $used_nodes_arr, 'tailoring variable', 'editing', 'Tailoring variable')), 'error');
  }
  $form_message = '';
  if ($count == 0) {
    field_attach_submit('tailoring_variable', $tv_submisttion, $form, $form_state);
    $tv = tailoring_variable_save($tv_submisttion);
    if (empty($form_state['values']['tailoring_variable_id'])) {
      $form_message = 'New tailoring variable %tailoring_variable_name has been created';
    }
    else {
      $form_message = 'Tailoring variable %tailoring_variable_name has been edited';
    }
    drupal_set_message(t($form_message, array('%tailoring_variable_name' => $tv->tailoring_variable_name)));
    if ($form_state['values']['op'] == $form_state['values']['submit_1']) {
      $form_state['redirect'] = 'admin/structure/tailoring_variable/manage';
    }
    else {
      $form_state['redirect'] = 'admin/structure/tailoring_variable/' . $tv->tailoring_variable_id . '/thresholds/add';
    }
  }
}

/**
 * Function used for defining errors and warning messages during the edit/deletion of tailoring variable.
 */
function _createErrorAndWarningMessages($data_obj, $used_nodes_arr, $v_type, $operation, $obj_type) {
  if (is_array($used_nodes_arr) && !empty($used_nodes_arr)) {
    $node_titles = array();
    $node_objs = node_load_multiple(array_keys($used_nodes_arr));
    if (count($node_objs) > 1) {
      foreach ($node_objs as $each_node) {
        $node_titles[] = '<cite><strong>' . $each_node->title . '</strong></cite>';
      }
      $content_no = 'contents ';
    }
    else {
      $single_node_obj = array_shift($node_objs);
      $node_titles[] = '<cite><strong>' . $single_node_obj->title . '</strong></cite>';
      $content_no = 'content ';
    }
    if ($obj_type == 'Tailoring variable') {
      $obj_name = $data_obj->tailoring_variable_name;
    }
    elseif ($obj_type == 'Threshold') {
      $obj_name = $data_obj->threshold_name;
    }
    $error_msg_for_nodes = $obj_type . ' <strong>' . $obj_name . '</strong> is already used by tailored page ' . rtrim($content_no) . ' ';
    $node_titles = implode(' and ', array_filter(array_merge(array(implode(', ', array_slice($node_titles, 0, -1))), array_slice($node_titles, -1))));
    $error_msg_for_nodes .= t('!node_titles. Please delete above ' . $content_no . ' before @operation this @v_type.',
      array('!node_titles' => $node_titles, '@operation' => $operation, '@v_type' => $v_type)
    );

    return $error_msg_for_nodes;
  }
}

/**
 * Function used for deleting tailoring variable.(used as a call back fuction in tailoring_variables_menu function.).
 */
function delete_tailoring_variable($form, &$form_state) {
  if (is_numeric(arg(3)) && arg(4) == 'delete') {
    $tv_obj = tailoring_variable_load(arg(3));
    $used_nodes_arr = _check_tailoring_variable_usage($tv_obj);
    if (is_array($used_nodes_arr) && !empty($used_nodes_arr)) {
      drupal_set_message(filter_xss(_createErrorAndWarningMessages($tv_obj, $used_nodes_arr, 'tailoring variable', 'deleting', 'Tailoring variable')), 'error');
      drupal_goto('admin/structure/tailoring_variable/manage');
    }
    else {
      $breadcrumb = array();
      $breadcrumb[] = l(t('Home'), '<front>');
      $breadcrumb[] = l(t('Administration'), 'admin');
      $breadcrumb[] = l(t('Structure'), 'admin/structure');
      $breadcrumb[] = l(t('Tailoring Variables'), 'admin/structure/tailoring_variable/manage');
      drupal_set_breadcrumb($breadcrumb);
      $form['tailoring_variable_id'] = array('#type' => 'value', '#value' => $tv_obj->tailoring_variable_id);

      return confirm_form($form,
        t('Are you sure you want to delete %tailoring_variable_name and its thresholds?', array('%tailoring_variable_name' => $tv_obj->tailoring_variable_name)),
        array('path' => 'admin/structure/tailoring_variable/manage'),
        t('This action cannot be undone.'),
        t('Delete'),
        t('Cancel')
      );
    }
  }
}

/**
 * Function used for checking the existing tailoring variable's usage(done this checking during edit/delete  tailoring variables).
 */
function _check_tailoring_variable_usage($value_obj) {
  if (isset($value_obj->tailoring_variable_id)) {
    $query = db_select('tailoring_variables', 'tvbs')->distinct('tvbs.nid');
    $query->join('tailored_message_group', 'tmg', 'tvbs.nid = tmg.nid');
    $query->join('tailored_variation', 'tv', 'tmg.tailored_message_group_id = tv.tailored_message_id');
    $query->join('tailored_variation_values', 'tvv', 'tv.tailored_variation_id = tvv.tailored_variation_id');
    $query->fields('tvbs', array('nid'));
    $query->condition('tvv.variable', $value_obj->tailoring_variable_id, '=');
    if (isset($value_obj->threshold_id)) {
      $query->condition('tvv.value', $value_obj->threshold_id, '=');
    }
    $result = $query->execute()->fetchAllAssoc('nid');
    if (is_array($result)) {
      return $result;
    }
    else {
      return '';
    }
  }
}

/**
 * Function used for deleting the  tailoring variable.
 */
function delete_tailoring_variable_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $tv_obj = tailoring_variable_load($form_state['values']['tailoring_variable_id']);
    tailoring_variable_delete($tv_obj);
    drupal_set_message(t('The tailoring variable %tailoring_variable_name has been deleted',
        array('%tailoring_variable_name' => $tv_obj->tailoring_variable_name, '%tailoring_variable_id' => $tv_obj->tailoring_variable_id)
      )
    );
    $form_state['redirect'] = 'admin/structure/tailoring_variable/manage';
  }
}

/**
 * Function used for deleting the threshold.(used as a page argument in tailoring_variables_menu function.).
 */
function delete_threshold($form, &$form_state) {
  if (arg(2) == 'threshold' && is_numeric(arg(3)) && arg(4) == 'delete') {
    $thd_obj = variable_thresholds_load(arg(3));
    $used_nodes_arr = _check_tailoring_variable_usage($thd_obj);
    if (is_array($used_nodes_arr) && !empty($used_nodes_arr)) {
      drupal_set_message(filter_xss(_createErrorAndWarningMessages($thd_obj, $used_nodes_arr, 'threshold', 'deleting', 'Threshold')), 'error');
      drupal_goto('admin/structure/tailoring_variable/' . $thd_obj->tailoring_variable_id);
    }
    else {
      $breadcrumb = array();
      $breadcrumb[] = l(t('Home'), '<front>');
      $breadcrumb[] = l(t('Administration'), 'admin');
      $breadcrumb[] = l(t('Structure'), 'admin/structure');
      $breadcrumb[] = l(t('Tailoring Variables'), 'admin/structure/tailoring_variable/manage');
      $tv_obj = tailoring_variable_load($thd_obj->tailoring_variable_id);
      $tv_name = $tv_obj->tailoring_variable_name;
      $breadcrumb[] = l($tv_name, 'admin/structure/tailoring_variable/' . $thd_obj->tailoring_variable_id . '/thresholds');
      drupal_set_breadcrumb($breadcrumb);
      $form['threshold_id'] = array('#type' => 'value', '#value' => $thd_obj->threshold_id);

      return confirm_form($form,
        t('Are you sure you want to delete %threshold ?', array('%threshold' => $thd_obj->threshold_name)),
        array('path' => 'admin/structure/tailoring_variable/' . $thd_obj->tailoring_variable_id . '/thresholds'),
        t('This action cannot be undone.'),
        t('Delete'),
        t('Cancel')
      );
    }
  }
}

/**
 * Function used for editing the threshold.(used as a page argument in tailoring_variables_menu function.).
 */
function edit_threshold($form, &$form_state) {
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Administration'), 'admin');
  $breadcrumb[] = l(t('Structure'), 'admin/structure');
  $breadcrumb[] = l(t('Tailoring Variables'), 'admin/structure/tailoring_variable/manage');
  drupal_add_css(drupal_get_path('module', 'tailoring_variables') . '/tailoring_variables.css');
  if (arg(2) == 'threshold' && is_numeric(arg(3)) && arg(4) == 'edit' && is_numeric(arg(5))) {
    $thd_obj = variable_thresholds_load(arg(3));
    $tv_obj = tailoring_variable_load($thd_obj->tailoring_variable_id);
    $tv_name = $tv_obj->tailoring_variable_name;
    $breadcrumb[] = l($tv_name, 'admin/structure/tailoring_variable/' . $thd_obj->tailoring_variable_id);
    drupal_set_breadcrumb($breadcrumb);
    $used_nodes_arr = _check_tailoring_variable_usage($thd_obj);
    if (is_array($used_nodes_arr) && !empty($used_nodes_arr)) {
      $node_objs = node_load_multiple(array_keys($used_nodes_arr));
      if (count($node_objs) > 1) {
        foreach ($node_objs as $each_node) {
          $node_titles[] = '<cite><strong>' . $each_node->title . '</strong></cite>';
        }
        $content_no = 'contents ';
      }
      else {
        $single_node_obj = array_shift($node_objs);
        $node_titles[] = '<cite><strong>' . $single_node_obj->title . '</strong></cite>';
        $content_no = 'content ';
      }
      $node_titles = implode(' and ', array_filter(array_merge(array(implode(', ', array_slice($node_titles, 0, -1))), array_slice($node_titles, -1))));
      $warning_msg_for_nodes = t('<strong>' . $thd_obj->threshold_name . '</strong> threshold is already used by tailored page ' . rtrim($content_no) . ' !node_titles.', array('!node_titles' => $node_titles));
      drupal_set_message(filter_xss($warning_msg_for_nodes), 'warning');
    }
    $lbl = 'Threshold : ' . arg(5);
    $form['thresholds']['description'] = array(
      '#type' => 'item',
      '#description' => t('&nbsp; Enter value ranges with hyphen difference.(Ex: 1-12)'),
    );
    $form['thresholds']['data'] = array(
      '#type' => 'item',
      '#prefix' => '<div class="threshold-grid">',
      '#suffix' => '</div>',
      '#id' => 'delta',
    );
    $form['thresholds']['data']['threshold_id'] = array(
      '#type' => 'value',
      '#value' => $thd_obj->threshold_id,
    );
    $form['thresholds']['data']['variable_id'] = array(
      '#type' => 'value',
      '#value' => $thd_obj->tailoring_variable_id,
    );
    $form['thresholds']['data']['row_number'] = array(
      '#type' => 'item',
      '#title' => t('&nbsp;&nbsp;' . $lbl),
      '#size' => 10,
    );
    $form['thresholds']['data']['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#size' => 10,
      '#default_value' => $thd_obj->threshold_values,
    );
    $form['thresholds']['data']['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#size' => 10,
      '#default_value' => $thd_obj->threshold_name,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    $form['cancel'] = array(
      '#type' => 'submit',
      '#access' => TRUE,
      '#value' => 'Cancel',
      '#weight' => 60,
      // This is the name of a function reproduced below
      '#submit' => array('edit_threshold_form_cancel'),
    );

    return $form;
  }
}

/**
 * Function used for canceling the threshold edit.
 */
function edit_threshold_form_cancel() {
  $thd_obj = variable_thresholds_load(arg(3));
  // replace with a URL if there is not a destination parameter.
  $backurl = 'admin/structure/tailoring_variable/' . $thd_obj->tailoring_variable_id;
  drupal_goto($backurl);
}

/**
 * Function used for validating add/edit threshold form fields.
 */
function edit_threshold_validate($form, &$form_state) {
  $dt_submission = $form_state['values'];
  if ($dt_submission['key'] == '' || $dt_submission['value'] == '') {
    form_set_error('', 'Threshold fields cannot be blank.');
  }
}

/**
 * Function used for validating add/edit threshold form fields.
 */
function edit_threshold_submit($form, &$form_state) {
  $variable_id = $form_state['values']['variable_id'];
  $uid = $GLOBALS['user']->uid;
  $dt_submission = $form_state['values'];
  if ($dt_submission['key'] != '' && $dt_submission['value'] != '') {
    $thd_obj = _create_object_foreach_threshold($dt_submission, $variable_id, $uid);
    field_attach_submit('variable_thresholds', $thd_obj, $form, $form_state);
    $thd = variable_thresholds_save($thd_obj);
    drupal_set_message(t('Threshold has been updated'));
    $form_state['redirect'] = 'admin/structure/tailoring_variable/' . $variable_id;
  }
}

/**
 * Function used for submitting add/edit threshold form.
 */
function delete_threshold_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $thd_obj = variable_thresholds_load($form_state['values']['threshold_id']);
    threshold_delete($thd_obj);
    drupal_set_message(t('The threshold %threshold (ID %threshold_id) has been deleted',
        array('%threshold' => $thd_obj->threshold_name, '%threshold_id' => $thd_obj->threshold_id)
      )
    );
    $form_state['redirect'] = 'admin/structure/tailoring_variable/' . $thd_obj->tailoring_variable_id . '/thresholds';
  }
}

/**
 * Function used for setting title during tailoring variable edit.
 */
function set_title_for_edit_tailoring_variable($arg) {
  $tv_obj = tailoring_variable_load($arg);

  return 'Edit ' . $tv_obj->tailoring_variable_name;
}

/**
 * Function used for setting title during threshold edit.
 */
function set_title_for_edit_thresholds($arg) {
  $tv_obj = tailoring_variable_load($arg);

  return 'Edit Thresholds for ' . '"' . $tv_obj->tailoring_variable_name . '"';
}

/**
 * Function used for setting title during threshold add.
 */
function set_title_for_add_thresholds($arg) {
  $tv_obj = tailoring_variable_load($arg);

  return 'Add New Thresholds for ' . '"' . $tv_obj->tailoring_variable_name . '"';
}

/**
 * Function used for creating form for add thresholds.
 */
function add_thresholds($form, &$form_state) {
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Administration'), 'admin');
  $breadcrumb[] = l(t('Structure'), 'admin/structure');
  $breadcrumb[] = l(t('Tailoring Variables'), 'admin/structure/tailoring_variable/manage');

  drupal_add_css(drupal_get_path('module', 'tailoring_variables') . '/tailoring_variables.css');
  $add_edit_lbl = '';
  $tailoring_variable = tailoring_variable_load(arg(3));
  $tv_name = $tailoring_variable->tailoring_variable_name;
  $breadcrumb[] = l($tv_name, 'admin/structure/tailoring_variable/' . $tailoring_variable->tailoring_variable_id);
  drupal_set_breadcrumb($breadcrumb);
  $form = array();
  $form['thresholds'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<div id="thresholds">',
    '#suffix' => '</div>',
  );
  $add_edit_lbl = 'Save Thresholds';
  $form['thresholds']['mode'] = array(
    '#type' => 'value',
    '#value' => 'add',
  );
  $form_state['storage']['thresholds'] = isset($form_state['storage']['thresholds']) ? $form_state['storage']['thresholds'] : 1;
  if ($form_state['storage']['thresholds']) {
    $form['thresholds']['data']['notice'] = array(
      '#type' => 'item',
      '#description' => t('&nbsp; Enter value ranges with hyphen difference.(Ex: 1-12)'),
    );
    for ($i = 0; $i < $form_state['storage']['thresholds']; ++$i) {
      $cnt = $i + 1;
      $lbl = 'Threshold : ' . $cnt;
      $form['thresholds']['data'][$i] = array(
        '#type' => 'item',
        '#prefix' => '<div class="threshold-grid">',
        '#suffix' => '</div>',
        '#id' => 'delta' . $i,
      );
      $form['thresholds']['data'][$i]['hid_cnt'] = array(
        '#type' => 'hidden',
        '#value' => $cnt,
      );
      $form['thresholds']['data'][$i]['row_number'] = array(
        '#type' => 'item',
        '#title' => t('&nbsp;&nbsp;' . $lbl),
        '#size' => 10,
      );
      $form['thresholds']['data'][$i]['value'] = array(
        '#type' => 'textfield',
        '#title' => t('Value'),
        '#size' => 10,
      );
      $form['thresholds']['data'][$i]['key'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#size' => 10,
      );
      if ($form_state['storage']['thresholds'] > 1 && $i == $form_state['storage']['thresholds'] - 1) {
        $form['thresholds']['data'][$i]['remove_name'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#submit' => array('remove_threshold'),
          '#ajax' => array(
            'callback' => 'add_threshold',
            'wrapper' => 'thresholds',
          ),
        );
      }
    }
  }
  $form['add_thresholds'] = array(
    '#type' => 'submit',
    '#value' => t('Add more'),
    '#href' => '',
    '#submit' => array('add_more_threshold'),
    '#ajax' => array(
      'callback' => 'add_threshold',
      'wrapper' => 'thresholds',
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t($add_edit_lbl),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#access' => TRUE,
    '#value' => 'Cancel',
    '#limit_validation_errors' => array(),
    '#weight' => 60,
    // This is the name of a function reproduced below
    '#submit' => array('add_threshold_form_cancel'),
  );

  return $form;
}

/**
 * Function used for canceling the threshold add.
 */
function add_threshold_form_cancel() {
  $tailoring_variable = tailoring_variable_load(arg(3));
  // replace with a URL if there is not a destination parameter.
  $backurl = 'admin/structure/tailoring_variable/' . $tailoring_variable->tailoring_variable_id;
  drupal_goto($backurl);
}

/**
 * Function used for removing the threshold row from form.
 */
function remove_threshold($form, &$form_state) {
  if ($form_state['storage']['thresholds'] > 0) {
    --$form_state['storage']['thresholds'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Function used for add more threshold to form.
 */
function add_more_threshold($form, &$form_state) {
  ++$form_state['storage']['thresholds'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Function used to add threshold.
 */
function add_threshold($form, $form_state) {
  return $form['thresholds'];
}

/**
 * Function used for validating threshold.
 */
function add_thresholds_validate($form, &$form_state) {
  $error_msg = '';
  $t_name_arr = array();
  $thd_names = '';
  $t_values = array();
  $th_values = '';
  drupal_add_css(drupal_get_path('module', 'tailoring_variables') . '/tailoring_variables.css');
  $tv_obj = tailoring_variable_load(arg(3));
  if (isset($form_state['values']['thresholds'])) {
    $threshold_values = $form_state['values']['thresholds'];
    foreach ($threshold_values['data'] as $key => $each_threshold) {
      if ($each_threshold['key'] != '' && $each_threshold['value'] != '') {
        $val_result = _validate_threshold_for_duplicate_name($each_threshold['key'], $each_threshold['value'], $tv_obj);
        if ($val_result === TRUE) {
          $t_name_arr[] = $each_threshold['key'];
        }
        elseif ($val_result != '') {
          $t_name_arr[] = $each_threshold['key'];
          $t_values[] = $each_threshold['value'];
        }
      }
      else {
        if ($form_state['clicked_button']['#value'] == 'Save Thresholds' || $form_state['clicked_button']['#value'] == 'Add more') {
          if ($each_threshold['key'] == '' || $each_threshold['value'] == '') {
            form_set_error('', filter_xss('Threshold :' . $each_threshold['hid_cnt'] . ' fields cannot be blank.'));
          }
        }
      }
    }
    if (!empty($t_name_arr) && empty($t_values)) {
      $error_msg .= 'Threshold name(s) ';
      $thd_names = implode(',', $t_name_arr);
      $error_msg .= t('<cite><strong>' . $thd_names . '</cite></strong> already associated with tailoring variable <strong>' . $tv_obj->tailoring_variable_name . ' </strong>. Use different name...');
      form_set_error('', filter_xss($error_msg));
    }
    else {
      if (!empty($t_values)) {
        $thr_lbl = '';
        $thd_names = implode(',', $t_name_arr);
        $t_values = implode(',', $t_values);
        if (preg_match('/-/i', $t_values)) {
          $thr_lbl = 'Threshold range';
        }
        else {
          $thr_lbl = 'Threshold value(s)';
        }
        $error_msg .= t($thr_lbl . ' <cite><strong>' . $t_values . '</cite></strong> defined for <cite><strong>' . $thd_names . '</cite></strong> threshold(s) are already used by other thresholds of <strong>' . $tv_obj->tailoring_variable_name . '</strong>.Please use different.');
        form_set_error('', filter_xss($error_msg));
      }
    }
  }
}

/**
 * Function used for validating duplicate threshold name.
 */
function _validate_threshold_for_duplicate_name($thd_name, $thd_value, $tv_obj) {
  $new_thd_name = _create_machine_name($thd_name);
  $current_thresholds = threshold_set($tv_obj);
  if (is_array($current_thresholds)) {
    $thd_objs = variable_thresholds_load_multiple(array_keys($current_thresholds));
    foreach ($thd_objs as $key => $each_threshold_obj) {
      if ($new_thd_name == $each_threshold_obj->threshold_name) {
        return TRUE;
      }
      else {
        if (is_numeric($thd_value)) {
          if ($each_threshold_obj->threshold_values == $thd_value) {
            return $thd_value;
          }
        }
        else {
          if (preg_match('/-/i', $thd_value)) {
            $thd_arr = explode('-', $thd_value);
            $range_arr_for_thd = range($thd_arr[0], $thd_arr[1]);
            if (preg_match('/-/i', $each_threshold_obj->threshold_values)) {
              $thd_arr_for_each_threshold_obj = explode('-', $each_threshold_obj->threshold_values);
              $range_arr_for_each_threshold_obj = range($thd_arr_for_each_threshold_obj[0], $thd_arr_for_each_threshold_obj[1]);
              $result = array_intersect($range_arr_for_each_threshold_obj, $range_arr_for_thd);
              if (!empty($result)) {
                return $thd_value;
              }
            }
          }
        }
      }
    }
  }

  return FALSE;
}

/**
 * Function used for submitting threshold data.
 */
function add_thresholds_submit($form, &$form_state) {
  $tv_obj = tailoring_variable_load(arg(3));
  $uid = $GLOBALS['user']->uid;
  if (isset($form_state['values']['thresholds'])) {
    $thd_names = array();
    $threshold_values = $form_state['values']['thresholds'];
    $msg_add_edit = $threshold_values['mode'] == 'edit' ? 'updated' : 'added';
    foreach ($threshold_values['data'] as $key => $each_threshold) {
      if (($each_threshold['key'] != '') && ($each_threshold['value'] != '')) {
        $thd_obj = _create_object_foreach_threshold($each_threshold, $tv_obj->tailoring_variable_id, $uid);
        field_attach_submit('variable_thresholds', $thd_obj, $form, $form_state);
        $thd = variable_thresholds_save($thd_obj);
        $thd_names[] = $thd->threshold_name;
      }
    }
    $thd_names = implode(',', $thd_names);
    $success_msg = '%thd_names threshold(s) has been ' . $msg_add_edit . ' for tailoring variable %tailoring_variable_name';
    drupal_set_message(t($success_msg, array('%tailoring_variable_name' => $tv_obj->tailoring_variable_name, '%thd_names' => $thd_names)));
    $form_state['redirect'] = 'admin/structure/tailoring_variable/' . $tv_obj->tailoring_variable_id;
  }
}

/**
 * Function used for creating threshold object patteren.
 */
function _create_object_foreach_threshold($each_threshold, $tvid, $uid) {
  $thd_o = (object)array(
    'threshold_id' => isset($each_threshold['threshold_id']) ? $each_threshold['threshold_id'] : '',
    'tailoring_variable_id' => $tvid,
    'threshold_name' => $each_threshold['key'],
    'threshold_machine_name' => _create_machine_name($each_threshold['key']),
    'threshold_values' => $each_threshold['value'],
    'created' => isset($each_threshold['threshold_id']) ? '' : REQUEST_TIME,
    'modified' => isset($each_threshold['threshold_id']) ? REQUEST_TIME : '',
    'uid' => $uid,
  );

  return $thd_o;
}

