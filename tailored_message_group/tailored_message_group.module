<?php

/**
 * @file
 * Implementes the tailored message group functionality required to create an entity and display it.
 */
module_load_include('php', 'tailored_message_group', 'tailored_message_ajax_add_more');

/**
 * Implements hook_entity_info().
 *
 * We let Drupal know about our entity via this functions - have a look at
 * http://api.drupal.org/api/drupal/modules--system--system.api.php/function/hook_entity_info/7
 * for all the information.
 * Below we do the tailored message group to get an entity with one bundle going.
 */
function tailored_message_group_entity_info() {
  $tailored_message_group_info['tailored_message_group'] = array(
    'label' => t('Tailored Message Group'),
    'controller class' => 'TailoredMessageGroupController',
    'base table' => 'tailored_message_group',
    'uri callback' => 'tailored_message_group_uri',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'tailored_message_group_id',
    ),
    'static cache' => TRUE,
    'bundles' => array(
      'tailored_message_group' => array(
        'label' => 'Tailored Message Group',
        'admin' => array(
          'path' => '',
          'access arguments' => array(
            'administer tailored_message_group entities',
          ),
        ),
      ),
    ),
    // View modes allow entities to be displayed differently based on context. We simply have one option
    // here but an alternative would be to have a Full and Teaser mode akin to node.
    'view modes' => array(
      'full' => array(
        'label' => t('Full'),
        'custom settings' => FALSE,
      ),
    ),
  );

  return $tailored_message_group_info;
}

/**
 * Fetch a tailored message group object. Make sure that the wildcard you choose
 * in the tailored message group entity definition fits the function name here.
 *
 * @param $tailored message group_id
 * Integer specifying the tailored message group entity id.
 * @param $reset
 * A boolean indicating that the internal cache should be reset.
 *
 * @return
 * A fully-loaded $tailored_message_group object or FALSE if it cannot be loaded.
 *
 * @see tailored_message_group_load_multiple()
 */
function tailored_message_group_load($tailored_message_group_id = NULL, $reset = FALSE) {
  $tailored_message_group_ids = (isset($tailored_message_group_id) ? array(
      $tailored_message_group_id,
    ) : array());
  $tailored_message_group = tailored_message_group_load_multiple(
    $tailored_message_group_ids, $reset
  );

  return $tailored_message_group ? reset($tailored_message_group) : FALSE;
}

/**
 * Loads nultiple tailored message group entities based on certain conditions.
 */
function tailored_message_group_load_multiple(
  $tailored_message_group_ids = array(), $conditions = array(), $reset = FALSE
) {
  return entity_load('tailored_message_group', $tailored_message_group_ids, $conditions, $reset);
}

/**
 * Implementing the uri callback defined.
 */
function tailored_message_group_uri($tailored_message_group) {
  return array(
    'path' => 'tailored_message_group/' .
    $tailored_message_group->tailored_message_group_id,
  );
}

/**
 * Implements hook_menu().
 */
function tailored_message_group_menu() {
  $items = '';

  return $items;
}

/**
 * Implements hook_permission().
 */
function tailored_message_group_permission() {
  return array(
    'administer tailored_message_group entities' => array(
      'title' => t('Administer tailored message group entities'),
      'restrict access' => TRUE,
    ),
    'view tailored_message_group entities' => array(
      'title' => t('View Tailored Message Group Entities'),
    ),
    'create tailored_message_group entities' => array(
      'title' => t('Create Tailored Message Group Entities'),
    ),
    'delete tailored_message_group entities' => array(
      'title' => t('Delete Tailored Message Group Entities'),
    ),
    'manage tailored_message_group entities' => array(
      'title' => t('Manage Tailored Message Group Entities'),
    ),
  );
}

/**
 * Callback for title.
 */
function tailored_message_group_page_title($tailored_message_group) {
  return $tailored_message_group->name;
}

/**
 * Callback for displaying the entity.
 */
function tailored_message_group_page_view($tailored_message_group, $view_mode = 'full') {
  $tailored_message_group->content = array();
  // Build fields content - this where the FieldAPI really comes in to play. The task
  // is relatively trivial for us - it all gets taken care of by Drupal core.
  field_attach_prepare_view('tailored_message_group',
    array(
      $tailored_message_group->tailored_message_group_id => $tailored_message_group,
    ), $view_mode
  );
  entity_prepare_view('tailored_message_group',
    array(
      $tailored_message_group->tailored_message_group_id => $tailored_message_group,
    )
  );
  $tailored_message_group->content += field_attach_view(
    'tailored_message_group', $tailored_message_group, $view_mode
  );
  drupal_set_title($tailored_message_group->name);

  return $tailored_message_group->content;
}

/**
 * Implemnts hook_field_extra_fields().
 *
 * This exposes the other column of our entity (i.e. name) as a pseudo-field
 * so that it gets handled by the Entity and Field core functionality.
 * Node titles get treated in a similar manner.
 */
function tailored_message_group_field_extra_fields() {
  $return = array();
  $return['tailored_message_group']['name'] = array(
    'form' => array(
      'name' => array(
        'label' => t('name'),
        'description' => t('Tailored Message Group Entity Name'),
      ),
    ),
  );

  return $return;
}

/**
 * Creates a tailored_message_group entity for us - simply intializing the main variables and
 * sending us to a form to add in the rest.
 */
function tailored_message_group_add() {
  $tailored_message_group = (object)array(
    'tailored_message_group_id' => '',
    'type' => 'tailored_message_group',
    'name' => '',
  );
  $nid = get_from_url(3);

  return drupal_get_form('tailored_message_group_add_form', $tailored_message_group, $nid);
}

/**
 * This function is used to set the node ID from url for tailored_message_group_add_form.
 */
function get_from_url($parameter_number) {
  $parameters = explode('/', $_GET['q']);
  if ($parameter_number < 1 || !is_int($parameter_number)) {
    thrownew Exception('Invalid Parameter');
  }
  //Offset by one
  return $parameters[$parameter_number - 1];
}

/**
 * Form callback: allows us to create a tailored message group entity.
 *
 * As you can see below the pattern followed is:
 * 1. Set up the form for the data that is specific to your
 * entity - typically the columns of your bas table.
 * 2. Call on the Field API to pull in the form elements
 * for fields attached to the entity.
 */
function tailored_message_group_add_form($form, &$form_state, $tailored_message_group, $nid) {
  $existing_tailored_messages = get_tailored_messages($nid);
  $form['description_markup'] = array(
    '#type' => 'markup',
    '#markup' => '<div>
     					Create a grouping of tailoring messages. A tailoring message group can then be assigned to a tailoring page, and rearranged on a tailoring page.
     			   	</div>',
  );
  $form['container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tailored Page'),
    '#prefix' => '<div id="tailored-messages-fieldset-wrapper">',
    '#suffix' => '</div>',
  );
  if (count($existing_tailored_messages) > 0) {
    _ajax_tailored_message_edit($form, $form_state, $nid, $existing_tailored_messages);
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => 'Update',
      '#submit' => array('tailored_message_group_add_form_update'),
    );
  }
  else {
    _ajax_tailored_message($form, $form_state, $nid);
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
    );
  }
  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('<< Back'),
    '#submit' => array('back_to_previous_page'),
    '#limit_validation_errors' => array(),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('back_to_content_list'),
    '#limit_validation_errors' => array(),
  );
  $form['container']['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  return $form;
}

/**
 * This function is used to navigate from add tailred variation page (second page) to tailoring variable selection page (first page).
 */
function back_to_previous_page(&$form, &$form_state) {
  if ($form_state['build_info']['form_id'] == 'tailored_message_group_add_form') {
    if (arg(1) == 'tailor' && is_numeric(arg(2))) {
      $_SESSION['messages'] = '';
      drupal_goto('/node/' . arg(2) . '/edit');
    }
  }
}

/**
 * This function is used to got back to main content listing page.
 */
function back_to_content_list(&$form, &$form_state) {
  if ($form_state['build_info']['form_id'] == 'tailored_message_group_add_form') {
    if (arg(1) == 'tailor' && is_numeric(arg(2))) {
      $_SESSION['messages'] = '';
      drupal_goto('/admin/content');
    }
  }
}

/**
 * This function is used for rendering add tailored variation message form with tailoring varaible threshold options.
 */
function _ajax_tailored_variation(&$form, &$form_state, $nid, $messaging_fieldset, $op_mode) {
  $msg_orginal_no = substr($messaging_fieldset, -1);

  $arg = arg(2);
  if (isset($arg)) {
    $selected = tailoring_selected_tailoring_variable_ids_for_node($arg);
    $form_state['storage']['var'] = $selected;
  }
  else {
    $selected = $form_state['storage']['var'];
  }
  $selected_tvs = explode(',', $selected);
  if (empty($form_state[$messaging_fieldset]['num_names'])) {
    $form_state[$messaging_fieldset]['num_names'] = 1;
  }
  for ($i = 0; $i < $form_state[$messaging_fieldset]['num_names']; ++$i) {
    $c = $i + 1;
    $form['container'][$messaging_fieldset]['variation_fieldset' . $i] = array(
      '#type' => 'fieldset',
      '#title' => t('Variation ' . $c),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#prefix' => '<div id="variation_fieldset' . $i . '">',
      '#suffix' => '</div>',
    );
    foreach ($selected_tvs as $key => $value) {
      $tv_obj = tailoring_variable_load($value);
      if (is_object($tv_obj)) {
        $thd_id_objs = threshold_set($tv_obj);
        if ($thd_id_objs != 'NULL') {
          $thd_objs = variable_thresholds_load_multiple(array_keys($thd_id_objs));
          $thrds = tailoring_thresholds_for_display($thd_objs);
          $form['container'][$messaging_fieldset]['variation_fieldset' . $i]['tailored_variables'][$value] = array(
            '#type' => 'checkboxes',
            '#title' => $tv_obj->tailoring_variable_name,
            '#options' => $thrds,
            '#prefix' => '<div>',
            '#suffix' => '</div>',
          );
        }
      }
    }
    $form['container'][$messaging_fieldset]['variation_fieldset' . $i]['variation'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
    );
  }
  if ($form_state['num_messages'] - 1 == $msg_orginal_no && $op_mode == 'new') {
    $form['container'][$messaging_fieldset]['add_name'] = array(
      '#type' => 'submit',
      '#value' => t('Add Variation'),
      '#submit' => array(
        'tailored_message_group_add_more_add_one',
      ),
      '#ajax' => array(
        'callback' => 'tailored_message_group_add_more_callback',
        'wrapper' => $messaging_fieldset,
      ),
      '#attributes' => array(
        'class' => array(
          $messaging_fieldset,
        ),
      ),
    );
    if ($form_state[$messaging_fieldset]['num_names'] > 1) {
      $form['container'][$messaging_fieldset]['remove_name'] = array(
        '#type' => 'submit',
        '#value' => t('Remove Variation'),
        '#submit' => array(
          'tailored_message_group_add_more_remove_one',
        ),
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'callback' => 'tailored_message_group_add_more_callback',
          'wrapper' => $messaging_fieldset,
        ),
        '#attributes' => array(
          'class' => array(
            $messaging_fieldset,
          ),
        ),
      );
    }
  }
  //End Ajax Example
}

/**
 * This function is used for rendering tailored variation message edit form with varaible threshold options.
 */
function _ajax_tailored_variation_edit(&$form, &$form_state, $nid, $messaging_fieldset, $tailored_message_id) {
  $tailored_message_no = (int)(substr($messaging_fieldset, -1)) + 1;
  $arg = arg(2);
  if (isset($arg)) {
    $selected = tailoring_selected_tailoring_variable_ids_for_node($arg);
    $form_state['storage']['var'] = $selected;
  }
  else {
    $selected = $form_state['storage']['var'];
  }
  $selected_tvs = explode(',', $selected);
  $variations = get_tailored_variations($tailored_message_id);
  $form_state[$messaging_fieldset]['num_names'] = count($variations);
  $i = 0;
  if (!empty($variations)) {
    foreach ($variations as $variation) {
      $c = $i + 1;
      $form['container'][$messaging_fieldset]['variation_fieldset' . $i] = array(
        '#type' => 'fieldset',
        '#title' => t('Variation ' . $c),
        '#collapsible' => TRUE,
      );
      foreach ($selected_tvs as $key => $value) {
        $tv_obj = tailoring_variable_load($value);
        if (is_object($tv_obj)) {
          $thd_id_objs = threshold_set($tv_obj);
          if ($thd_id_objs != 'NULL') {
            $thd_objs = variable_thresholds_load_multiple(array_keys($thd_id_objs));
            $thrds = tailoring_thresholds_for_display($thd_objs);
            $form['container'][$messaging_fieldset]['variation_fieldset' . $i]['tailored_variables'][$value] = array(
              '#type' => 'checkboxes',
              '#title' => $tv_obj->tailoring_variable_name,
              '#options' => $thrds,
              '#default_value' => get_values($variation->tailored_variation_id, $selected),
              '#prefix' => '<div>',
              '#suffix' => '</div>',
            );
          }
        }
      }
      $form['container'][$messaging_fieldset]['variation_fieldset' . $i]['variation'] = array(
        '#type' => 'textarea',
        '#title' => t('Message'),
        '#default_value' => $variation->variation,
      );
      $form['container'][$messaging_fieldset]['variation_fieldset' . $i]['tailored_variation_id'] = array(
        '#type' => 'hidden',
        '#value' => $variation->tailored_variation_id,
      );
      $form['container'][$messaging_fieldset]['variation_fieldset' . $i]['delete'] = array(
        '#type' => 'checkbox',
        '#title' => 'Delete Variation ' . $c . ' | Tailored Message ' . $tailored_message_no,
        '#return_value' => $variation->tailored_variation_id,
      );
      ++$i;
    }
  }
  else {
    $tailored_message_title = _getTailoredMessageTitleForAllVariationsDeleted($tailored_message_id, $nid);
    drupal_set_message(t('No variations found for tailored message <strong>%tailred_message_title</strong>. Please add variations for this  tailored message.', array('%tailred_message_title' => $tailored_message_title)), 'warning');
    _ajax_tailored_variation($form, $form_state, $nid, $messaging_fieldset, 'edit');
  }
  //End Ajax Example
}

/**
 * This function returns the name of the varaition message title  of the varaiton message to which all of its variations are deleted .this title is used for wanrning display in the variation message add/edit form .
 */
function _getTailoredMessageTitleForAllVariationsDeleted($tailored_message_id, $nid) {
  $existing_tailored_messages = get_tailored_messages($nid);
  if (count($existing_tailored_messages) > 0) {
    foreach ($existing_tailored_messages as $tailored_message) {
      if ($tailored_message->tailored_message_group_id == $tailored_message_id) {
        return $tailored_message->name;
      }
    }
  }
}

/**
 * This function is used for rendering the  tailred message add form .
 */
function _ajax_tailored_message(&$form, &$form_state, $nid) {
  $form['#tree'] = TRUE;
  // Build the fieldset with the proper number of names. We'll use
  // $form_state['num_names'] to determine the number of textfields to build.
  if (empty($form_state['num_messages'])) {
    $form_state['num_messages'] = 1;
  }
  $k = 1;
  for ($i = 0; $i < $form_state['num_messages']; ++$i) {
    $full_title = 'Tailored Message ' . $k;
    $form['container']['messages_fieldset' . $i] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => $full_title,
      '#prefix' => '<div id="messages_fieldset' . $i . '">',
      '#suffix' => '</div>',
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    );
    $form['container']['messages_fieldset' . $i]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#required' => TRUE,
      '#description' => t('A name or short description for your message.'),
    );
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    _ajax_tailored_variation($form, $form_state, $nid, 'messages_fieldset' . $i, 'new');
    ++$k;
  }
  $form['container']['add_message'] = array(
    '#type' => 'submit',
    '#value' => t('Add Message'),
    '#submit' => array(
      'tailored_message_add_more_add_one',
    ),
    // See the examples in tailored_message_group.module for more details on the
    // properties of #ajax.
    '#ajax' => array(
      'callback' => 'tailored_message_add_more_callback',
      'wrapper' => 'tailored-messages-fieldset-wrapper',
    ),
  );
  if ($form_state['num_messages'] > 1) {
    $form['container']['remove_message'] = array(
      '#type' => 'submit',
      '#value' => t('Remove Message'),
      '#submit' => array(
        'tailored_message_add_more_remove_one',
      ),
      '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => 'tailored_message_add_more_callback',
        'wrapper' => 'tailored-messages-fieldset-wrapper',
      ),
    );
  }
  //End Ajax Example
}

/**
 * This function is used for rendering the  tailred message edit form .
 */
function _ajax_tailored_message_edit(&$form, &$form_state, $nid, $existing_tailored_messages) {
  $form['#tree'] = TRUE;
  $form_state['num_messages'] = count($existing_tailored_messages);
  $i = 0;
  $j = 1;
  foreach ($existing_tailored_messages as $tailored_message) {
    $form['container']['messages_fieldset' . $i] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('Tailored Message ' . $j),
      '#prefix' => '<div id="messages_fieldset' . $i . '">',
      '#suffix' => '</div>',
    );
    $form['container']['messages_fieldset' . $i]['tailored_message_id'] = array(
      '#type' => 'hidden',
      '#value' => $tailored_message->tailored_message_group_id,
    );
    $form['container']['messages_fieldset' . $i]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#required' => TRUE,
      '#default_value' => $tailored_message->name,
      '#description' => t('A name or short description for your message.'),
    );
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    _ajax_tailored_variation_edit($form, $form_state, $nid, 'messages_fieldset' . $i, $tailored_message->tailored_message_group_id);
    $form['container']['messages_fieldset' . $i]['delete_message'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete Tailored Message ' . $j),
      '#return_value' => $tailored_message->tailored_message_group_id,
    );
    ++$i;
    ++$j;
  }
  $form['#submit'] = array('tailored_message_group_add_form_submit');
}

/**
 * Form callback: submits tailored_message_group_add_form information.
 */
function tailored_message_group_add_form_update($form, &$form_state) {
  $_SESSION['messages'] = '';
  $values = array_shift($form_state['values']);
  foreach ($values as $value_key => $tailored_message_value) {
    // This ensures that only the fieldsets are added.
    if (strpos($value_key, 'fieldset') != FALSE) {
      if (isset($tailored_message_value['delete_message']) && $tailored_message_value['delete_message'] > 0) {
        _deleteTailoredMessageGroup($tailored_message_value, $values['nid'], $values[$value_key]['tailored_message_id']);
      }
      else {
        $name = $tailored_message_value['name'];
        $tailored_message = _create_tailored_message($name, $values['nid'], 0, $values[$value_key]['tailored_message_id']);
        field_attach_submit('tailored_message', $tailored_message, $form, $form_state);
        $tailored_message = tailored_message_group_update($tailored_message, $values[$value_key]['tailored_message_id']);
        foreach ($tailored_message_value as $variation_name => $variation) {
          if (strpos($variation_name, 'fieldset') != FALSE) {
            if (isset($variation['delete']) && $variation['delete'] > 0) {
              $tailored_variation = get_tailored_variation($variation['tailored_variation_id']);
              tailored_variation_delete($tailored_variation);
            }
            else {
              $tv_id = isset($variation['tailored_variation_id']) ? $variation['tailored_variation_id'] : '';
              $tailored_variation = _create_tailored_variation($name, $variation['variation'], $variation['tailored_variables'], $tailored_message->tailored_message_group_id, $tv_id);
              field_attach_submit('tailored_variation', $tailored_variation, $form, $form_state);
              tailored_variation_update($tailored_variation);
            }
          }
        }
      }
    }
  }
  $node_obj = node_load($values['nid']);
  $form_message = 'Tailored message(s) of <strong>%variation_node_title</strong> has been updated.';
  drupal_set_message(t($form_message, array('%variation_node_title' => $node_obj->title)), 'status');
  $existing_tailored_messages = get_tailored_messages($node_obj->nid);
  if (count($existing_tailored_messages) == 0) {
    drupal_set_message(t('All tailored messages of tailoring page <strong>%tailred_page_title</strong> have been deleted. Please add tailored message(s).', array('%tailred_page_title' => $node_obj->title)), 'warning');
  }
}

/**
 * This function is used for deleting single message group (now this function is called only when tailor page delete message checkbox is checked) .
 */
function _deleteTailoredMessageGroup($tailored_message_value, $nid, $tailored_message_group_id) {
  foreach ($tailored_message_value as $variation_name => $variation) {
    if (strpos($variation_name, 'fieldset') != FALSE && isset($variation['tailored_variation_id'])) {
      $tailored_variation = get_tailored_variation($variation['tailored_variation_id']);
      tailored_variation_delete($tailored_variation);
    }
  }
  $tailored_message_group_obj = _create_tailored_message($tailored_message_value['name'], $nid, 0, $tailored_message_group_id);
  tailored_message_group_delete($tailored_message_group_obj);
}

/**
 * Form callback: submits tailored_message_group_add_form information.
 */
function tailored_message_group_add_form_submit($form, &$form_state) {
  $values = array_shift($form_state['values']);
  foreach ($values as $value_key => $tailored_message_value) {
    // This ensures that only the fieldsets are added.
    if (strpos($value_key, 'fieldset') != FALSE) {
      $name = $tailored_message_value['name'];
      $tailored_message = _create_tailored_message($name, $values['nid']);
      field_attach_submit('tailored_message', $tailored_message, $form, $form_state);
      $tailored_message = tailored_message_group_save($tailored_message);
      foreach ($tailored_message_value as $variation_name => $variation) {
        if (strpos($variation_name, 'fieldset') != FALSE) {
          $tailored_variation = _create_tailored_variation($name, $variation['variation'], $variation['tailored_variables'], $tailored_message->tailored_message_group_id);
          field_attach_submit('tailored_variation', $tailored_variation, $form, $form_state);
          tailored_variation_save($tailored_variation);
        }
      }
    }
  }
  $node_obj = node_load($values['nid']);
  $form_message = 'Tailored message(s) for %variation_node_title has been created.';
  drupal_set_message(t($form_message, array('%variation_node_title' => $node_obj->title)), 'status');
}

/**
 * Private method that creates a tailored variation object from form values submitted to it.
 *
 * @returns tailored_variation object
 */
function _create_tailored_variation($name, $variation, $tailored_variables, $tailored_message_id, $tailored_variation_id = NULL) {
  $tailored_variation = (object)array(
    'tailored_variation_id' => $tailored_variation_id,
    'variation' => $variation,
    'tailored_message_id' => $tailored_message_id,
    'tailored_variables' => $tailored_variables,
  );

  return $tailored_variation;
}

/**
 * Private method that creates a tailored message object from form values submitted to it.
 *
 * @return tailored_message object
 */
function _create_tailored_message($name, $nid, $weight = 0, $tailored_message_id = FALSE) {
  $tailored_message = array(
    'name' => $name,
    'weight' => $weight,
    'nid' => $nid,
  );
  // perform update
  if ($tailored_message != FALSE) {
    $tailored_message['tailored_message_group_id'] = $tailored_message_id;
  }

  return (object) $tailored_message;
}

/**
 * Form callback: Validates tailored_message_group_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function tailored_message_group_add_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] != 'Remove Variation') {
    $tailored_message_group_submission = (object) $form_state['values'];
    $m_count = _number_of_form_post_messages($tailored_message_group_submission);
    $msg_delete_chk_value = 0;
    if ($m_count > 0) {
      $k = 1;
      for ($i = 0; $i < $m_count; ++$i) {
        $v_count = _number_of_variations($tailored_message_group_submission->container['messages_fieldset' . $i]);
        $msg_delete_chk_value = isset($tailored_message_group_submission->container['messages_fieldset' . $i]['delete_message']) ? $tailored_message_group_submission->container['messages_fieldset' . $i]['delete_message'] : 0;
        if ($v_count > 0) {
          for ($j = 0; $j < $v_count; ++$j) {
            $tailored_variables = $tailored_message_group_submission->container['messages_fieldset' . $i]['variation_fieldset' . $j]['tailored_variables'];
            $variation_message = $tailored_message_group_submission->container['messages_fieldset' . $i]['variation_fieldset' . $j]['variation'];
            $filter = array();
            foreach ($tailored_variables as $each_tv) {
              $select_dt = array_filter($each_tv);
              if (!empty($select_dt)) {
                $filter[] = $select_dt;
              }
            }
            $s = $j + 1;
            $error_msg = '';
            if ($msg_delete_chk_value == 0) {
              if (empty($filter) || empty($variation_message)) {
                if (empty($filter)) {
                  $error_msg .= t('Please select at least one tailoring variable from variation section ' . $s . ' for tailored message ' . $k . '<br/>');
                }
                if (empty($variation_message)) {
                  $error_msg .= t('Please Enter Variation from variation section ' . $s . ' for tailored message ' . $k);
                }
                form_set_error('threshold', $error_msg);
              }
            }
          }
        }
        ++$k;
      }
    }
    $tailored_message_group_submission = (object) $form_state['values'];
    field_attach_form_validate('tailored_message_group', $tailored_message_group_submission, $form, $form_state);
  }
}

/**
 * This function is used for returning posted messages count.
 */
function _number_of_form_post_messages($form_post_obj) {
  $keys = array_keys($form_post_obj->container);
  $msg_count = 0;
  foreach ($keys as $key) {
    if (preg_match('/messages_fieldset/', $key)) {
      ++$msg_count;
    }
  }

  return $msg_count;
}

/**
 * This function is used for returning variations count.
 */
function _number_of_variations($message) {
  $keys = array_keys($message);
  $v_count = 0;
  foreach ($keys as $key) {
    if (preg_match('/variation_fieldset/', $key)) {
      ++$v_count;
    }
  }

  return $v_count;
}

/**
 * We save the tailored_message_group entity by calling the controller.
 */
function tailored_message_group_save(&$tailored_message_group) {
  return entity_get_controller('tailored_message_group')->save($tailored_message_group);
}

/**
 * We save the tailored_message_group entity by calling the controller.
 */
function tailored_message_group_update(&$tailored_message_group) {
  return entity_get_controller('tailored_message_group')->update($tailored_message_group);
}

/**
 * We delete the tailored_message_group entity by calling the controller.
 */
function tailored_message_group_delete(&$tailored_message_group) {
  return entity_get_controller('tailored_message_group')->delete($tailored_message_group);
}

/**
 * Implements hook_help().
 */
function tailored_message_group_help($path, $arg) {
  switch ($path) {
    case 'admin/help#tailored_message_group':
      return '<p>' . t('Once you have activated the module you can configure your entity bundle by visiting "admin/structure/tailored_message_group/manage"') . '</p>';
  }
}

/**
 * This function is used to load tailored_message_group entity.
 */
function get_tailored_message_groups() {
  return entity_load('tailored_message_group', FALSE);
}

/**
 * Enter description here ...
 *
 * @param unknown_type $nid
 *
 * @return returns array['Pretty Name'] = machine_name
 */

/**
 * This method gets the tailored messages that belong to a certain node.
 */
function get_tailored_messages($nid) {
  //Construct a FieldQuery to select entities that belong to specific node.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'tailored_message_group', '=')->propertyCondition('nid', $nid, '=');
  $result = $query->execute();
  $ids = array();
  if (isset($result['tailored_message_group'])) {
    foreach ($result['tailored_message_group'] as $tailored_message_group) {
      $ids[] = $tailored_message_group->tailored_message_group_id;
    }
    $tailored_messages = entity_load('tailored_message_group', $ids);
  }
  else {
    $tailored_messages = array();
  }

  return $tailored_messages;
}

/**
 * BasicController extends the DrupalDefaultEntityController by adding
 * an extra function to handle saving of entities.
 */
class TailoredMessageGroupController extends DrupalDefaultEntityController {

  /**
   * Saves the tailored_message_group entity mathching custom fields via drupal_write_record().
   */
  public function save($tailored_message_group) {
    drupal_write_record('tailored_message_group', $tailored_message_group);
    field_attach_insert('tailored_message_group', $tailored_message_group);
    module_invoke_all('entity_insert', $tailored_message_group, 'tailored_message_group');

    return $tailored_message_group;
  }

  /**
   * This method updates the tailored_message_group .
   */
  public function update($tailored_message_group) {
    drupal_write_record('tailored_message_group', $tailored_message_group, 'tailored_message_group_id');
    field_attach_update('tailored_message_group', $tailored_message_group);
    module_invoke_all('entity_update', $tailored_message_group, 'tailored_message_group');

    return $tailored_message_group;
  }

  /**
   * Delete tailored_message_group.
   */
  public function delete($tailored_message_group) {
    $this->delete_multiple(array($tailored_message_group));
  }

  /**
   * Delete tailored_message_groups.
   */
  public function delete_multiple($tailored_message_groups) {
    if (!empty($tailored_message_groups)) {
      foreach ($tailored_message_groups as $tailored_message_group) {
        module_invoke_all('tailored_message_group_delete', $tailored_message_group);
        module_invoke_all('entity_delete', $tailored_message_group, 'tailored_message_group');
        field_attach_delete('tailored_message_group', $tailored_message_group);
        db_delete('tailored_message_group')->condition('tailored_message_group_id', $tailored_message_group->tailored_message_group_id, '=')->execute();
      }
    }
  }
}

