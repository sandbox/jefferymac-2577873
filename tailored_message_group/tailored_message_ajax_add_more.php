<?php

/**
 * @file
 * This file is used for defining the call back functions of ajax-enabled buttons .
 */

/**
 * This simply selects and returns the fieldset with the names in it.
 */
function tailored_message_group_add_more_callback($form, $form_state) {
  $messaging_fieldset = _ajax_get_class_attribute($form_state);

  return $form['container'][$messaging_fieldset];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * It just increments the max counter and causes a rebuild.
 */
function tailored_message_group_add_more_add_one($form, &$form_state) {
  $messaging_fieldset = _ajax_get_class_attribute($form_state);
  ++$form_state[$messaging_fieldset]['num_names'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Enter description here ...
 *
 * @param form_state
 */
function _ajax_get_class_attribute($form_state) {
  $messaging_fieldset = $form_state['triggering_element']['#attributes']['class'][0];

  return $messaging_fieldset;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function tailored_message_group_add_more_remove_one($form, &$form_state) {
  $messaging_fieldset = _ajax_get_class_attribute($form_state);
  if ($form_state[$messaging_fieldset]['num_names'] > 1) {
    --$form_state[$messaging_fieldset]['num_names'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Final submit handler.
 *
 * Reports what values were finally set.
 */
function tailored_message_group_add_more_submit($form, &$form_state) {
  $output = t('These people are coming to the picnic: @names',
    array(
      '@names' => implode(', ',
        $form_state['values']['messages_fieldset']['name']
      ),
    )
  );
  drupal_set_message($output);
}
// AJAX Callbacks for creating tailored message

/**
 * Callback for both ajax-enabled buttons.
 *
 * This simply selects and returns the fieldset with the names in it.
 */
function tailored_message_add_more_callback($form, $form_state) {
  return $form['container'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * It just increments the max counter and causes a rebuild.
 */
function tailored_message_add_more_add_one($form, &$form_state) {
  ++$form_state['num_messages'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function tailored_message_add_more_remove_one($form, &$form_state) {
  if ($form_state['num_messages'] > 1) {
    --$form_state['num_messages'];
  }
  $form_state['rebuild'] = TRUE;
}

