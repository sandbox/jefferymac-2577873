<?php

/**
 * @file
 * Install file for Tailoring Module
 */

/**
 * Implements hook_install().
 *
 * - Add the body field.
 * - Configure the body field.
 */
function tailoring_install() {
  $t = get_t();
  $tailoring = array(
    'type' => 'tailoring',
    'name' => $t('Tailored Page'),
    'base' => 'node_content',
    'description' => $t('A page that contains tailored messages'),
    'body_label' => $t('A page that contains tailored messages'),
  );
  // Complete the node type definition by setting any defaults not explicitly
  // declared above.
  // http://api.drupal.org/api/function/node_type_set_defaults/7
  $content_type = node_type_set_defaults($tailoring);
  // Save the content type
  node_type_save($content_type);
}

/**
 * Implements hook_schema().
 */
function tailoring_schema() {
  $schema = array();
  $schema['tailoring_variables'] = array(
    'description' => 'This table stores selected  tailoring variable ids of tailored page ',
    'fields' => array(
      'tailoring_variables_id' => array(
        'description' => 'Primary Key / ID',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'tailoring_variable_id_list' => array(
        'description' => 'The comma separated IDs  of the selected tailoring variables',
        'type' => 'varchar',
        'length' => '250',
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => 'Node ID of the tailored page - foreign key',
        'type' => 'int',
        'length' => 100,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('tailoring_variables_id'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function tailoring_uninstall() {
  // Gather all the example content that might have been created while this
  // module was enabled.  Simple selects still use db_query().
  // http://api.drupal.org/api/function/db_query/7
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => 'tailoring'));
  $nids = array();
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }
  // Delete all the nodes at once
  // http://api.drupal.org/api/function/node_delete_multiple/7
  node_delete_multiple($nids);
  // Loop over each of the fields defined by this module and delete
  // all instances of the field, their data, and the field itself.
  // http://api.drupal.org/api/function/field_delete_field/7
  /* foreach (array_keys(_tailoring_installed_fields()) as $field) {
   field_delete_field($field);
 }*/

  // Loop over any remaining field instances attached to the tailoring
  // content type (such as the body field) and delete them individually.
  // http://api.drupal.org/api/function/field_delete_field/7
  $instances = field_info_instances('node', 'tailoring');
  foreach ($instances as $instance_name => $instance) {
    field_delete_instance($instance);
  }
  // Delete our content type
  // http://api.drupal.org/api/function/node_type_delete/7
  node_type_delete('tailoring');
  $sub_module_list = _getSubModulesList();
  drupal_uninstall_modules($sub_module_list, $uninstall_dependents = FALSE);
  drupal_flush_all_caches();
  drupal_set_message(t('The sub modules tailored_variation, tailored_message_group and tailoring_variables were successfully uninstalled.'));
  // Purge all field infromation
  // http://api.drupal.org/api/function/field_purge_batch/7
  field_purge_batch(1000);
}

/**
 * Implements hook_disable().
 */
function tailoring_disable() {
  //$type_info = node_type_load('tailoring');
  //  uncomment the following lines to disable all sub modules when tailoring got disable
  $sub_module_list = _getSubModulesList();
  module_disable($sub_module_list, $disable_dependents = FALSE);
  //tailoring_disable_tailoring_content_type(1);
  drupal_flush_all_caches();
  drupal_set_message('The sub modules tailored_variation, tailored_message_group and tailoring_variables were disabled successfully.');
  // Here we may have to write code to disable/hide "Tailoring Page" content type
}

/**
 * This function return the sub module names as array (used by drupal_uninstall_modules function).
 */
function _getSubModulesList() {
  return array('tailoring_variables', 'tailored_variation', 'tailored_message_group');
}

/**
 * Implements hook_enable().
 */
function tailoring_enable() {
  //tailoring_disable_tailoring_content_type(0);
  //drupal_flush_all_caches();
  //$type_info = node_type_load('tailoring');
  //$type_info->disabled = 0;
  // node_type_save($type_info);
  //// Here we may have to write code to Enable "Tailoring Page" content type
}

